<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/about', function() {
    return view('about');
});

Route::get('/home', 'HomeController@index');
Route::get('/contact', function() {
    return view('contact');
});

Route::get('/test', 'CategoryController@test');
Route::get('/kategori', 'CategoryController@index');
Route::get('/kategori/create', 'CategoryController@create');
Route::post('/kategori/create', 'CategoryController@store');
Route::get('/kategori/{id_kategori}/edit' , 'CategoryController@edit');
Route::put('/kategori/{id_kategori}/edit' , 'CategoryController@update');
Route::delete('/kategori/{id_kategori}/delete', 'CategoryController@destroy');
Route::get('/kategori/{id_kategori}/show' , 'CategoryController@show');



// Route::get('/admin/categories', 'CategoryController@index');
// Route::get('/admin/categories/create', 'CategoryController@create');
// Route::post('/admin/categories', 'CategoryController@store');
// Route::get('/admin/categories/{id}/show', 'CategoryController@show');
// Route::get('/admin/categories/{id}/edit', 'CategoryController@edit');
// Route::put('/admin/categories/{id}', 'CategoryController@update');
// Route::delete('/admin/categories/{id}', 'CategoryController@destroy');

// Route Resources
// Route::resource('/admin/product', 'ProductController');
Route::get('/admin', function() {
    return view('admin-newest.layouts.app');
});

Route::get('/profile', function() {
    return view('profile');
});
