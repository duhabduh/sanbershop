@extends('admin-newest.layouts.app')

@section('title', 'Category - Edit')

@section('content')
  <form class="" action="{{ url('/kategori/' . $kategori->id. '/edit') }}" method="post">
    <input type="text" class="form-control" name="nama_kategori" placeholder="isi nama kategori" 
    value=" {{ $kategori->nama_kategori }}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <input type="submit" class="btn btn-success" name="" value="Update Kategori">
  </form>
@endsection